
# @Redirect
Redirect lets us fully replace method calls with our own.

## Sample Class
```java
package com.biom4st3r.sample;

public class SampleClass
{

    private final Singleton singleton = singletons.OXYGEN;
    private int cooldown;
    private final int MAXSIZE = 64;

    public Singleton getSingleton()
    {
        return this.singleton;
    }

    public void sampleMethod(Planet planet, Person person, int i, boolean bl)
    {
        if (this.cooldown > 0) {
            --this.cooldown;
        }

        if (this.getSingleton() != null) {
            this.getSingleton().inventoryTick(this, planet, person, i, bl);
        }
    }
}
```

So what if we want to watch the world burn and crashing the game instead of letting the inventory tick?

```java
@Redirect(method = {"sampleMethod"},at = @At(value = "INVOKE",target = "com/biom4st3r/sample/Singleton.inventoryTick(Lcom/biom4st3r/sample/SampleClass;Lcom/biom4st3r/sample/Planet;Lcom/biom4st3r/sample/Person;IZ)V"),ordinal = 0)
public void containerTock(Singleton singleton,Planet planet, Person person, int i, boolean z)
{
    if(singleton == singletons.OXYGEN)
    {
        throw new Exception("Good bye");
    }
}
```
``ordinal`` is VERY important in Redirect. If you do not declare an ``ordinal`` **ALL CALLS** to the target within this ``method`` will be replaced.

a ``@Redirect`` method must match the bytecode local variables of our target method. So let's look at the bytecode of our ``sampleMethod`` :
```
public sampleMethod(com.biom4st3r.sample.Planet arg0, com.biom4st3r.sample.Person arg1, int arg2, boolean arg3) { //(Lcom/biom4st3r/sample/Planet;Lcom/biom4st3r/sample/Person;IZ)V
    <localVar:index=0 , name=this , desc=Lcom/biom4st3r/sample/SampleClass;, sig=null, start=L1, end=L2>
    <localVar:index=1 , name=planet , desc=Lcom/biom4st3r/sample/Planet;, sig=null, start=L1, end=L2>
    <localVar:index=2 , name=person , desc=Lcom/biom4st3r/sample/Person;, sig=null, start=L1, end=L2>
    <localVar:index=3 , name=i , desc=I, sig=null, start=L1, end=L2>
    <localVar:index=4 , name=bl , desc=Z, sig=null, start=L1, end=L2>
```
you'll see that the first local variable is ``this`` of type SampleClass. That is because this is an instance method and instance methods load a reference to their host objects first. So because we are redirecting Singleton#inventoryTick we'll be needing a reference to that Singleton.

So what did we just do? we've told mixin to create a new method named "containerTock" inside of ``Singleton`` which the same signature as our target. and then completely replaced the original call to ``inventoryTick``. So the ``invokevirtual`` in our [sample method bytecode](/SampleMethodByteCode.md) in ``L6`` becomes:

```invokevirtual com/biom4st3r/sample/Singleton.containerTock(Lcom/biom4st3r/sample/SampleClass;Lcom/biom4st3r/sample/Planet;Lcom/biom4st3r/sample/Person;IZ)V```

so our sample method now looks like this(approx).
```java
public void sampleMethod(Planet planet, Person person, int i, boolean bl)
{
    if (this.cooldown > 0) {
        --this.cooldown;
    }

    if (this.getSingleton() != null) {
        this.getSingleton().containerTock(this, planet, person, i, bl);
    }
}
```
maybe instead of ``containerTick`` we want to redirect ``getSingleton``. We have to change the ``at`` to ``@At(value = "INVOKE", target = "com/biom4st3r/sample/SampleClass.getSingleton()Lcom/biom4st3r/sample/Singleton;")`` and change our method to:

```java
public Singleton getSomethingElse(SampleClass sampleClass)
{
    return singletons.OXYGEN;
}
```
then our ``sampleMethod`` would look like this:
```java
public void sampleMethod(Planet planet, Person person, int i, boolean bl)
{
    if (this.cooldown > 0) {
        --this.cooldown;
    }

    if (this.getSomethingElse() != null) {
        this.getSomethingElse().inventoryTick(this, planet, person, i, bl);
    }
}
```
# Notes
* You should try and use ``Inject`` before ``Redirect``, because it causes fewer compatibility problems. You are fully replacing method calls that other modders could have used as targets.
* Only 1 mod can redirect anyone call. All others will crash.

