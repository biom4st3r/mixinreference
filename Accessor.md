# Accessors
Accessors are just a shortcut for duck typing. They let you create Getters and Setters for fields inside of your targeted class by forcing the Class to implement your Mixin Class, then automatically creating the method bodies based on the signature of your method. ``Getters`` have a non-void return type and no arguments and ``Setters`` has a void return type with 1 argument.
```java
package com.biom4st3r.sample;

public class SampleClass
{

    private final Singleton singleton = singletons.OXYGEN;
    private int cooldown;
    private final int MAXSIZE = 64;

    public Singleton getSingleton()
    {
        return this.singleton;
    }

    public void sampleMethod(Planet planet, Person person, int i, boolean bl)
    {
        if (this.cooldown > 0) {
            --this.cooldown;
        }

        if (this.getSingleton() != null) {
            this.getSingleton().inventoryTick(this, planet, person, i, bl);
        }
    }
}
```
Let's say we want to do some shenanigans and mutate our SampleClass however we want. We can implement Setters and Getters for those 3 fields like this:
```java

@Mixin(SampleClass.java)
public interface ExposedSampleClass
{
    @Accessor("singleton") @Mutable
    public void setSingleton(Singleton singleton);

    @Accessor("MAXSIZE")
    public int getMaxSize();

    @Accessor("MAXSIZE") @Mutable
    public void setMaxSize(int i); // Please read the note on @Mutable before you do this on a primitive type like int.

    @Accessor("cooldown")
    public void setCoolDown(int i);

}
```
**Note on @Mutable**
The mutable annotation is used to tell Mixin that you know this field is ``final`` and you want to change it. Remember, final is only a suggestion...unless the next note applies

**Note on @Mutable** You have to be careful when using a ``@Mutable`` accessor to change primitive types(``int``, ``double``, ``float``, ``boolean``...). When you compile java with final primitives the value will likely be inlined to optimize and because ``Compile Time`` comes before ``Runtime`` you'll likely not make any difference at all. For an example of inlining check out a decompiled version of ``com.mojang.blaze3d.systems.RenderSystem.DEFAULTALPHACUTOFF`` you'll see that it looks like it's never used, but ``RenderSystem.defaultAlphaFunc()`` uses the value, just inlined.

So in creating this Accessor our SampleClass now looks like this(approx).
```java
public class SampleClass implements ExposedSampleClass
{
    private Singleton singleton = singletons.OXYGEN;
    private int cooldown;
    private int MAXSIZE = 64;

    ...

    public void setSingleton(Singleton singleton)
    {
        this.singleton = singleton;
    }

    public int getMaxSize()
    {
        return this.MAXSIZE;
    }

    public void setMaxSize(int i)
    {
        this.MAXSIZE = i;
    }

    public void setCoolDown(int i)
    {
        this.cooldown = i;
    }
}
```
But you can't just use ``sampleClass.getMaxSize()``, because these methods don't exist until runtime when Mixin adds them. This is where Duck typing comes in.``((ExposedSampleClass)sampleClass).getMaxSize()`` By casting to your mixin interface you can access the implemented methods just fine.

# Notes 