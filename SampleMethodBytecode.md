# Sample Method Bytecode
```java
public sampleMethod(com.biom4st3r.sample.Planet arg0, com.biom4st3r.sample.Person arg1, int arg2, boolean arg3) { //(Lcom/biom4st3r/sample/Planet;Lcom/biom4st3r/sample/Person;IZ)V
    <localVar:index=0 , name=this , desc=Lcom/biom4st3r/sample/SampleClass;, sig=null, start=L1, end=L2>
    <localVar:index=1 , name=planet , desc=Lcom/biom4st3r/sample/Planet;, sig=null, start=L1, end=L2>
    <localVar:index=2 , name=person , desc=Lcom/biom4st3r/sample/Person;, sig=null, start=L1, end=L2>
    <localVar:index=3 , name=i , desc=I, sig=null, start=L1, end=L2>
    <localVar:index=4 , name=bl , desc=Z, sig=null, start=L1, end=L2>

    L1 {
        aload0 // reference to self
        getfield com/biom4st3r/sample/SampleClass.cooldown:int
        ifle L3
    }
    L4 {
        aload0 // reference to self
        dup
        getfield com/biom4st3r/sample/SampleClass.cooldown:int
        iconst_1
        isub
        putfield com/biom4st3r/sample/SampleClass.cooldown:int
    }
    L3 {
        f_new (Locals[5]: com/biom4st3r/sample/SampleClass, com/biom4st3r/sample/Planet, com/biom4st3r/sample/Person, 1, 1) (Stack[0]: null)
        aload0 // reference to self
        invokevirtual com/biom4st3r/sample/SampleClass.getSingleton()Lcom/biom4st3r/sample/Singleton;
        ifnull L5
    }
    L6 {
        aload0 // reference to self
        invokevirtual com/biom4st3r/sample/SampleClass.getSingleton()Lcom/biom4st3r/sample/Singleton;
        aload0 // reference to self
        aload1 // reference to arg0
        aload2 // reference to arg1
        iload3 // reference to arg2
        iload4
        invokevirtual com/biom4st3r/sample/Singleton.inventoryTick(Lcom/biom4st3r/sample/SampleClass;Lcom/biom4st3r/sample/Planet;Lcom/biom4st3r/sample/Person;IZ)V
    }
    L5 {
        f_new (Locals[0]: null) (Stack[0]: null)
        return
    }
    L2 {
    }
}
```