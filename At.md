# @At
```
@At(value = "INVOKE",target="",shift = Shift.AFTER,ordinal = 1)
```
The @At annotation is used to define a location to affect. The ``value`` arg defines a type of location such as `HEAD`(Top of the method), `TAIL`(Bottom of the method), and others. You can check the [@At javadocs](https://github.com/SpongePowered/Mixin/blob/1252714215e03bdca681e9841475d326cdeef475/src/main/java/org/spongepowered/asm/mixin/injection/At.java) for more information.

``target`` is used for ``INVOKE``, ``INVOKE_ASSIGN``, ``FIELD``, ``NEW``, and ``INVOKE_STRING`` to define a method invokation or a field assignment as the location.

``shift`` defines an offset to the default position after a valid location is found.

``ordinal`` picks which occurrence of the target opcode you are referencing in numerical order.

``slice`` (TODO)

``opcode`` specifies which instruction you are referring to. Example: you're targeting that ``ifnull`` jump in ``sampleMethod`` you'd use ``Opcodes.IFNULL`` and it would be `ordinal = 0` instead of it being `ordinal = 1`.

# Notes
* The default value for ``ordinal`` targets every invokation of the target
* shift doesn't care about your source code. It doesn't shift anything(necessarily) by ``X`` lines of source code. it shifts it ``X`` opcodes. Say we make an injection targeting ``getSingleton`` ``ordinal=0`` and then ``shift = Shift.BY`` ``by = 2``. our injection is now after the jump call ``ifnull L5`` so if ``singleton`` is null our code never gets called.

