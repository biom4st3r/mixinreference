# Bytecode References

## Bytecode signatures
You may notice some of those random letters thrown around in the bytecode like ``I``,``Z``,``V`` and such. Those are primitive Fully-qualified names; the same as ``Lcom/biom4st3r/sample/Singleton``.

| Symbol| Type     |
| - |:-------------|
| I | int          |
| Z | boolean      |
| V | void/no type |
| J | long         |
| F | float        |
| D | double       |
| B | byte         |
| C | char         |
| S | short        |
| [ | array        |
The array symbol will be followed by any other type