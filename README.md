# Preface
* compiletime -> runtime -> mixintime
* Fields created in Mixin classes can't be accessed without [ducktyping](/DuckTyping.md) a Getter/Setter, because they only exist past mixintime
* You can not cast to Mixin classes. They don't exist. They are only used as templates for modifying your target class.
* ^ Accessors and Invokers are special. Because they are just a shortcut for duck typing you can cast to those.
* Accessors and Invokers **must** be in an ``interface`` not a ``class``.

# Mixin Reference

[Duck Typing](/DuckTyping.md) - Work from within a class

[@At](/At.md) - Selecting targets

[Bytecode References](/Bytecode.md)

// TODO
 [@Slice](/Slice.md) - Safer more specific targeting

[@Accessor](/Accessor.md) - Make Getters/Setters for inaccessable fields

[@Invoker](/Invoker.md) - Make calls to inaccessable methods/ctors

[@Inject](/Inject.md) - Add code to existing code

[@Redirect](/Redirect.md) - Replace calls to existing Methods

[@ModifyArg](/ModifyArg.md) - change an argument passed to a method invokation

// TODO
 [@ModiftArgs](/ModifyArgs.md) - change multiple arguments passed to a method invokation

// TODO
 [@ModifyConstant](/ModifyConstant.md) - change magic numbers and the like

// TODO
 [@ModifyVariable](/ModifyVariable.md)

[@Overwrite]() - Ask in the Fabric discord. This is a last case no-other-options option

# Appendix
ctor - constructor

invokation - invocation

# Links
[Mixin Wiki](https://github.com/SpongePowered/Mixin/wiki) - better explanation of how Mixin works.

[Mixin Code](https://github.com/SpongePowered/Mixin) - javadocs in source code

[Fabric Discord](https://discord.gg/z3uFHZM) - for help

# Fields

Some commonly used annotations for Fields are 
```
@Shadow @Final @Mutable @Unique
private Singleton singleton;
```
I have these listed for awareness, not explanation. For more information please read the mixin javadocs.



