# Duck typing 
Duck typing is when you refer to an object by one of its superclasses. Mixin allows you to add additional interfaces to target classes

## Sample Class
```java
public class Person
{
    protected double x;
    protected double y;
    protected double z;
}
```

Now let's say that we want to be able to change a ``person``'s position. from outside of the class. We COULD make 3 accessors for settings the x, y, and z or we could just add a new method to ``Person`` let's name it teleport. First, we have to make an ``interface`` with our new method: 
## Sample interface that we want to apply
```java
public interface ExposedPerson
{

    void teleport(double x,double y, double z);

}
```
Then, we just need to implement our ``interface`` and define our new method: 

```java
@Mixin(Person.class)
public abstract class PersonMixin implements ExposedPerson
{
    @Shadow
    protected double x;
    @Shadow
    protected double y;
    @Shadow
    protected double z;

    @Override
    public void teleport(double x,double y, double z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }
}
```
Now, because Mixin runs at runtime we still won't be able to call ``person.teleport()`` in source code, because it doesn't exist at compiletime and attempting to do so will make the compiler angry. We can, however, duck type the person with ``((ExposedPerson)person).telport``, because at mixintime the interface and implementation of ``teleport`` were added to the target class.

# Notes
* You can get access to methods and fields that your target inherits by inheriting the same supers. if ``Person`` happened to inherit ``LivingPerson`` my mixin could also inherit ``LivingPerson``
* The ``abstract`` keyword in my class is optional, but it lets you not implement an abstract or undefined methods
* if you use the same inheritance at your Target and you have to make a ctor you shouldn't add any code to the ctor. It's just there to make the compiler happy.