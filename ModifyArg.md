# ModifyArg
ModifyArg allows you to replace a value that is passed to a method invokation with your method.
```java
public class BadEnemy
{
    Predicate<LivingPerson> VALID_GOAL = (lp)-> lp instanceof Person;
    protected void addGoals()
    {
        this.add(new AttackTarget(20.0D,VALID_GOAL));
    }
}
```
So right now that enemy attacks everyone, but I don't want them to attack me because I'm busy. So we need to tell Mixin to target the invokation of ``AttackTarget``'s ctor and replace the 2nd argument with our value.
```java
@Mixin(BadEnemy.class)
public class BadEnemyMxn
{
    @ModifyArg(method = "addGoals",at = @At(value = "INVOKE",target="com/biom4st3r/sample/AttackTarget.<init>(DLjava/util/function/Predicate;)V"),index = 1)
    public Predicate<LivingPerson> newValue(Predicate<LivingPerson> oldValue)
    {
        return (livingperson)-> oldValue.test(livingperson) && ((Person)livingperson).getName()!="biom4st3r";
    }
}
```

So with our ``@ModifyArgs`` were changing that method to look like this(appox)

```java
...
    protected void addGoals()
    {
        this.add(new AttackTarget(20.0D,newValue(VALID_GOAL)));
    }
...
```

# Notes 