# Invokers

Invokers are a shortcut for duck typing the same as Accessors. They let you force your target class to implement your mixin, then they are implements in the class to call the method specified. In the NewSampleClass you wouldn't be able to call either of the methods, because they are protected/private. 
## Sample Class
```java
public class NewSampleClass
{
    Singleton singleton = singletons.EXOTIC_OXYGEN;

    protected void setSingleton(Singleton singleton)
    {
        this.singleton = singleton;
    }
    
    private static void doStuff()
    {
        System.out.println("did stuff");
    }
}
```
```java
@Mixin(NewSampleClass.class)
public interface NewSampleClassInvoker
{
    @Invoker("setSingleton(Lcom/biom4st3r/sample/SingleTon)V")
    void setSingleton(Singleton singleton);
    
    @Invoker("doStuff()V")
    static void doMoreStuff()
    {
		// this body is ignored, but required for proper java syntax.
        throw new NotImplementedException("");
    }
}
```

Similarly to Accessors: Invokers modify the targets class and add a new method for each Invoker you define, by forcing the class to implement ``NewSampleClassInvoker``. 
So not our class ``NewSampleClass`` now looks like this:
```java
public class NewSampleClass implements NewSampleClassInvoker
{
    Singleton singleton = singletons.EXOTIC_OXYGEN;

    protected void setSingleton(Singleton singleton)
    {
        this.singleton = singleton;
    }
    
    private static void doStuff()
    {
        System.out.println("did stuff");
    }
	
	public void setSingleton(Singleton singleton)
	{
	this.setSingleTon(singleton);
	}
	
	public static void doMoreStuff()
	{
	this.doStuff();
	}
}
```
Just like Accessors, you can't just call those methods, because they're not in bytecode until runtime, so you have to cast to the Invoker
```
NewSampleClass nsc = new NewSampleClass();
((NewSampleClassInvoker)nsc).setSingleton(singletons.OXYGEN)
or
NewSampleClassInvoker.doMoreStuff();
```

# Notes
* You don't have to match the entire method signature IF there are not other methods with the same name, so the 2 targets above could be ``setSingleton`` and ``doStuff``.

