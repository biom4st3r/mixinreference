
# @Inject
Inject allows you to place your code inside of existing methods.

## Sample Class
```java
package com.biom4st3r.sample;

public class SampleClass
{

    private final Singleton singleton = singletons.OXYGEN;
    private int cooldown;
    private final int MAXSIZE = 64;

    public Singleton getSingleton()
    {
        return this.singleton;
    }

    public void sampleMethod(Planet planet, Person person, int i, boolean bl)
    {
        if (this.cooldown > 0) {
            --this.cooldown;
        }

        if (this.getSingleton() != null) {
            this.getSingleton().inventoryTick(this, planet, person, i, bl);
        }
    }
}
```
[Bytecode](/SampleMethodByteCode.md) if you need it

So let's say we don't want old people to get their inventory tick. We'd need to inject before when inventory tick gets called and return from the method if the person's age to greater than, say, 99.

```java
@Mixin(SampleClass.class)
public class SampleClassMxn
{
    @Inject(
        method = {"sampleMethod"},
        at = @At(
            value = "INVOKE", 
            target = "com/biom4st3r/sample/Singleton.inventoryTick(Lcom/biom4st3r/sample/SampleClass;Lcom/biom4st3r/sample/Planet;Lcom/biom4st3r/sample/Person;IZ)V")
        cancellable = true
        )
    public void sampleInjection(Planet planet, Person person, int i, boolean bl,CallbackInfo ci) 
    { // Note. you don't need to have every arg of the target method, but you also can not skip arguments that you don't need
        if(person.age > 99)
        {
            ci.cancel();
        }
    }
}
```

``cancellable`` allows you to 'return' from a method, though ``return`` statements are not permitted inside of ``@Inject`` so you have to use your ``CallbackInfo#cancel()`` to return from that method.

So what did we just do? We told Mixin to create a new method inside of ``SampleClass`` called ``sampleInjection``, then inject a call to our new method into ``sampleMethod`` **before** Singleton#inventoryTick. Before is bolded because the default behavior of INVOKE is shift = Shift.BEFORE (for more info please refer to [@At](/At.md)). so our ``sampleMethod`` now looks like this(approx)
```java
public void sampleMethod(Planet planet, Person person, int i, boolean bl)
{
...
    if (this.getSingleton() != null) {
        this.samepleInjection(planet,person,i,bl);
        this.getSingleton().inventoryTick(this, planet, person, i, bl);
    }
}

```
We could use ``@At("HEAD")`` and get
```java
public void sampleMethod(Planet planet, Person person, int i, boolean bl)
{
    this.samepleInjection(planet,person,i,bl);
    if (this.cooldown > 0) {
    ...
}
``` 
or ``TAIL``
```java
public void sampleMethod(Planet planet, Person person, int i, boolean bl)
{
    ...
    if (this.getSingleton() != null) {
        this.getSingleton().inventoryTick(this, planet, person, i, bl);
    }
    this.samepleInjection(planet,person,i,bl);
}
```
or ``@At(value = "JUMP",opcode = Opcodes.IFNULL,shift = Shift.AFTER)``. Please refer to the bytecode section for context.
```java
public void sampleMethod(Planet planet, Person person, int i, boolean bl)
{
...
    if (this.getSingleton() != null) {
        this.samepleInjection(planet,person,i,bl);
        this.getSingleton().inventoryTick(this, planet, person, i, bl);
    }
}
```
or ``@At(value = "FIELD", opcode = Opcodes.PUTFIELD, target = "com/biom4st3r/sample/SampleClass.cooldown:int",shift = Shift.BEFORE),ordinal = 1``. Please refer to the bytecode section for context.
```java
public void sampleMethod(Planet planet, Person person, int i, boolean bl)
{
    if (this.cooldown > 0) {
        this.samepleInjection(planet,person,i,bl);
        --this.cooldown;
    }
    ...
}
```

# Notes
* method accepts a ``String[]`` allowing you to target multiple methods
* Don't inject as HEAD just to cancel the method. Overwrite it. Overwrite is bad, but at least it will crash and let other mods know why there's a compatibility problem
* When injecting into ctors you can only target ``RETURN`` and ``TAIL``, though you can redirect anything in a ctor.
* The return type of Injections are always **void** even if the return type of your target isn't void. If you're targeting a non-void return type you'd change your ``CallbackInfo`` to a ``CallbackInfoReturnable<ReturnType>``

